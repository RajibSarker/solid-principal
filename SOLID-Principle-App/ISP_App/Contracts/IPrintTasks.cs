﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ISP_App.Contracts
{
    public interface IPrintTasks
    {
        bool PrintContent(string content);
        //bool FaxContent(string content);
        bool ScanContent(string content);   
        bool PhotoCopyContent(string content);
        //bool PrintDuplexContent(string content);
    }
    public interface IFaxContent
    {
        bool FaxContent(string content);
    }
    public interface IPrintDuplexContent
    {
        bool PrintDuplexContent(string content);
    }
}
