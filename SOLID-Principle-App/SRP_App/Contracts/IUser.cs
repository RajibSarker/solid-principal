﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRP_App.Contracts
{
    public interface IUser
    {
        bool Login(string userName, string password);
        bool Register(string userName, string password, string Email);
        //bool LogError(string errorMessage);
        //bool SendEmail(string emailContent);
    }
    public interface ILogger
    {
        bool LogError(string errorMessage);
    }
    public interface IEmailSender
    {
        bool SendEmail(string emailContent);
    }
}
